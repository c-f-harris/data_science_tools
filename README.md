
# Usage
- Change LoadData.py to get your data. It should return (data,labels) tuple.
- Change labels_to_color() to return a color for each label (used in the plots) 
- Call python main.py -args (where -args are all your arguments)

# Visualize
- "-pl", "--print_labels"                  ---   Print the label vector
- "-p", "--print"                          ---   Print all the data
- "-pca3", "--plot_3d"                     ---   Plot the data with labels in 3D
- "-pca2", "--plot_2d"                     ---   Plot the data with labels in 2D
- "-tsne", "--plot_manifold_tsne"          ---   Plot the data in 2D using manifold learning

# Clustering
- "-km", "--kmeans"                        ---   Print out the cluster centers using KMeans
- "-ac", "--agglomerative_clustering"      ---   Print number of cluster centers found w/ agglomerative_clustering
- "-sc", "--spectral_clustering"           ---   Assigns each feature vector a label using spectral clustering

# Analysis
- "-scp", "--spectral_clustering_then_pca" ---   Gets predicted labels using spectral clustering then plots the data with these labels in 2D using PCA
- "-psc", "--pca_then_spectral_clustering" ---   Does PCA to reduce data to 2D. Then does spectral clustering to get predicted labels and plots with labels.
