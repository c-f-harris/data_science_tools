
import argparse
from Visualize import *
from Cluster import *
from Analysis import *
from LoadData import get_data

def labels_to_color(labels):
	return "b"

def parse_arguments(data, labels):

	targets = labels_to_color(labels)
	parser = argparse.ArgumentParser(description="===== DATA SCIENCE TOOLS =====")

	# Visualize
	parser.add_argument("-pl", "--print_labels", action="store_true")
	parser.add_argument("-pca3", "--plot_3d", action="store_true")
	parser.add_argument("-pca2", "--plot_2d", action="store_true")
	parser.add_argument("-tsne", "--plot_manifold_tsne", action="store_true")
	# Clustering
	parser.add_argument("-km", "--kmeans", action="store_true")
	parser.add_argument("-ac", "--agglomerative_clustering", action="store_true")
	parser.add_argument("-sc", "--spectral_clustering", action="store_true")
	# Analysis:
	parser.add_argument("-scp", "--spectral_clustering_then_pca", action="store_true")
	parser.add_argument("-psc", "--pca_then_spectral_clustering", action="store_true")
	args = parser.parse_args()

	plt.ion()

	if args.print_labels:
		for l in labels: print(l)

	if args.plot_3d:
		plot_pca(data, dim=3, title="", targets=targets)

	if args.plot_2d:
		plot_pca(data, dim=2, title="", targets=targets)

	if args.plot_manifold_tsne:
		plot_tsne(data, title="")

	if args.kmeans:
		kmeans_test(data)

	if args.agglomerative_clustering:
		agglomerative_clustering(data)

	if args.spectral_clustering:
		spectral_clustering(data)

	if args.spectral_clustering_then_pca:
		plot_2d_with_spectral_clustering_labels(data, title="")

	if args.pca_then_spectral_clustering:
		pca_then_spectral_clustering(data, title="")

	x = input("\n\n\t* * * Enter to close windows * * * \n\n")



def main():
	data, labels = get_data()
	parse_arguments(data, labels)

if __name__ == "__main__":
	main()






#
