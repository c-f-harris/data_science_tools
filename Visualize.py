
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from sklearn import decomposition
from sklearn.manifold import TSNE

def plot_3d(x, y, z, targets="b", title=""):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x, y, z, c=targets, marker='o')
    if len(title)>0: plt.title(title)
    plt.show()

def plot_2d(x, y, targets="b", title=""):
    plt.figure()
    plt.scatter(x, y, c=targets)
    if len(title)>0: plt.title(title)
    plt.show()

def plot_pca(X, dim=3, title="", targets="b"):
    pca = decomposition.PCA(n_components=dim)
    pca.fit(X)
    X = pca.transform(X)
    if dim==3:
        plot_3d(X[:,0], X[:,1], X[:,2], title=title, targets=targets)
    else:
        plot_2d(X[:,0], X[:,1], title=title, targets=targets)

def plot_tsne(X, title=""):
    tsne = TSNE(random_state=0)
    result = tsne.fit_transform(X)
    plot_2d(result[:,0], result[:,1], title=title)

#
