
from Cluster import *
from Visualize import *

def plot_2d_with_spectral_clustering_labels(X, title=""):
    Y = spectral_clustering(X, print_output=False)
    plot_pca(X, dim=2, title="PCA after Spectral Clustering " + title, targets=Y)

def pca_then_spectral_clustering(X, title=""):
    pca = decomposition.PCA(n_components=2)
    pca.fit(X)
    X = pca.transform(X)
    Y = spectral_clustering(X, print_output=False)
    plot_2d(X[:,0], X[:,1], targets=Y, title="PCA before Spectral Clustering " + title)

def plot_pca_all_features(pc, pi, labels):
    X = np.column_stack([pc, pi])
    plot_pca(X, dim=2, title="PC + PI", targets=labels_to_color(labels))
