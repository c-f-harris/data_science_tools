
import numpy as np
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import SpectralClustering

def kmeans_test(X):
    kmeans = KMeans(n_clusters=5, random_state=0)
    kmeans.fit(X)
    print(f'\nCLUSTER CENTERS:\n\n{kmeans.cluster_centers_}\n\n')

def agglomerative_clustering(X, distance_threshold=1):
    agg = AgglomerativeClustering()
    agg.fit(X)
    print(agg.n_clusters)

def spectral_clustering(X, print_output=True):
    sc = SpectralClustering()
    Y = sc.fit_predict(X)
    if print_output:
        for i in range(len(X)):
            x,y = X[i], Y[i]
            print(f'x:\t{x}\n\ny:{y}\n\n ')
    return Y
